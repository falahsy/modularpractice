//
//  UseCaseType.swift
//  Core
//
//  Created by Syamsul Falah on 05/12/20.
//

import Foundation

public protocol UseCaseType {
  associatedtype Request
  associatedtype Response
  
  func execute(request: Request) -> Response
}
